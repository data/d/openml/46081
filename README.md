# OpenML dataset: Dog_Breeds_Ranked

https://www.openml.org/d/46081

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

### Description:

The Dogs Ranking Dataset is an informative compilation aimed at providing a comprehensive overview of various dog breeds and their characteristics. This dataset encompasses a wide array of attributes spanning from physical traits to cost-related aspects, making it an indispensable resource for potential dog owners, veterinarians, and dog enthusiasts. It delves into breed-specific information including type, size, intelligence, congenital ailments, and more, coupled with quantitative metrics suchasility for cohabitation with children, and financial commitments involved over a dog's lifespan.

### Attribute Description:

- **Breed**: Includes breeds like English Springer Spaniel, Akita, amongst others.
- **Type**: Classifies breeds into categories like hound, non-sporting, and terrier.
- **Score**: A numerical value indicating the breed's overall rating.
- **Popularity Ranking**: Numeric rank based on the breed's popularity.
- **Size & Size.1**: Represents the dog's physical stature, with values like medium and large. 'Size.1' offers a descriptive size categorization.
- **Intelligence**: Descriptive ranking of the breed's intelligence level.
- **Congenital Ailments**: Lists potential hereditary health issues.
- **Score for Kids**: Numerical rating reflecting suitability for families with children.
- **$LIFETIME COST**: Estimated financial cost covering the dog's lifespan.
- **INTELLIGENCE RANK & INTELLIGENCE %**: Provides a rank and percentage estimating the breed's intelligence.
- **LONGEVITY(YEARS)**: Average lifespan in years.
- **NUMBER OF GENETIC AILMENTS**: Indicates the count of known hereditary conditions.
- **GENETIC AILMENTS**: Detailing specific hereditary health issues.
- **PURCHASE PRICE**: Initial cost of acquiring the breed.
- **FOOD COSTS PER YEAR**: Annual estimated expenditure on food.
- **GROOMING FREQUENCY**: Required frequency of grooming.
- **SUITABILITY FOR CHILDS**: Numeric score indicating compatibility with children.

### Use Case:

This dataset is particularly valuable for individuals looking to adopt or purchase a dog by offering a data-driven approach to understanding breed-specific expectations and responsibilities. It assists in matching a breed to a potential owner's lifestyle, financial readiness, and compatibility with household members, including children. Additionally, it serves as a research tool for veterinary studies focusing on congenital ailments and their prevalence across breeds, thus contributing to targeted healthcare and breeding practices.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46081) of an [OpenML dataset](https://www.openml.org/d/46081). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46081/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46081/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46081/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

